/*!
 * Copyright (C) 2020 - All Rights Reserved by
 * @author : ZhaoYanbo
 * @email  : zyb920@hotmail.com
 * @created: 2020-10-31
 * @version: 1.0.0.0
 *
 */

#ifndef PARTICLETIMELINEWIDGET_H
#define PARTICLETIMELINEWIDGET_H

#include <QWidget>

struct ParticleItemData
{
public:
    bool toNext = false;
    int duration = 0;

    void reverseNext()
    {
        toNext = !toNext;
    }
};

class ParticleTimeLineWidget : public QWidget
{
    Q_OBJECT
public:
    ParticleTimeLineWidget(QWidget *parent = 0);
    ~ParticleTimeLineWidget();

    ///设置总时长
    void setEndDuration(int d);
    int endDuration() const { return m_endDuration; }

    ///设置当前时长
    void setCurDuration(int d);
    int curDuration() const { return m_curDuration; }

    ///
    void setPreviewEndDuration(int d);
    int previewEndDuration() const {
        return m_previewEndDuration;
    }

    bool setItemDuration(int index, int d);

    ///插入item
    bool insert(int index, ParticleItemData *item);

    ///选中某个
    void setSelected(int index);
    ///当前选中索引
    int selected() const { return m_selectedIndex; }

    ///设置勾选状态
    void setCanToNext(int index, bool ok);
    ///是否勾选
    bool canToNext(int index) const;

    ///移除某个(只剩下一个不能移除)
    bool remove(int index);
    ///移除当前选中的
    bool removeCurSelected();
    ///能否执行remove
    bool canRemove() const {
        return m_items.length() > 1;
    }

    ///移除索引0以外的所有
    void clear();
    ///能否执行清除
    bool canClear() const {
        return m_items.length() > 1;
    }

    ///检查点击在哪个数据块上
    int doCheckRect(const QPoint &qpos) const;

protected:
    void mousePressEvent(QMouseEvent *e) override;
    void mouseMoveEvent(QMouseEvent *e) override;
    void mouseReleaseEvent(QMouseEvent *e) override;
    void mouseDoubleClickEvent(QMouseEvent *e) override;

    void paintEvent(QPaintEvent *e) override;

private:
    void drawRuler(QPainter *p, int w, int height);

private:
    bool m_bPressed = false;
    int m_pressedPosX;
    int m_pressedDuration;

    int m_endDuration;
    int m_curDuration; //用于标尺
    int m_previewEndDuration; //预览结束时长
    int m_selectedIndex = -1;
    int m_hoverIndex = -1;

    int m_space;

    QList<ParticleItemData *> m_items;
};

#endif // PARTICLETIMELINEWIDGET_H
