# ParticleTimeLine

#### 一、介绍
一款简单的动作时间线编辑。


操作：双击界面插入对应的块，选中块可以左右移动。

#### 二、效果图
![image](img.png)


#### 三、联系方式
邮箱：zyb920@hotmail.com

QQ:  408815041


# 打赏使我更有动力
![image](https://gitee.com/zyb920/image-cache/raw/master/wx.png)

![image](https://gitee.com/zyb920/image-cache/raw/master/zfb.png)
